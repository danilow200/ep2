/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author danilo
 */
public class Preco extends javax.swing.JFrame {

    /**
     * Creates new form Preco
     */
    public Preco() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TextoPainelPreco = new javax.swing.JLabel();
        TextoTipoDoVeiculo = new javax.swing.JLabel();
        TipoDoVeiculos = new javax.swing.JComboBox<>();
        BotaoSalvar2 = new javax.swing.JButton();
        TextoPreco = new javax.swing.JLabel();
        Preco = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TextoPainelPreco.setText("Painel de preço");

        TextoTipoDoVeiculo.setText("Tipo do Veiculo");

        TipoDoVeiculos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Carro", "Moto", "Caminhão", "Van" }));

        BotaoSalvar2.setText("Salvar");
        BotaoSalvar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSalvar2ActionPerformed(evt);
            }
        });

        TextoPreco.setText("Preço por Km (R$00,00)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(151, 151, 151)
                .addComponent(TextoPainelPreco)
                .addContainerGap(153, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(BotaoSalvar2))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(TextoTipoDoVeiculo)
                                .addComponent(TipoDoVeiculos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Preco)
                                .addComponent(TextoPreco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TextoPainelPreco)
                .addContainerGap(268, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap(81, Short.MAX_VALUE)
                    .addComponent(TextoTipoDoVeiculo)
                    .addGap(18, 18, 18)
                    .addComponent(TipoDoVeiculos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(TextoPreco)
                    .addGap(18, 18, 18)
                    .addComponent(Preco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(23, 23, 23)
                    .addComponent(BotaoSalvar2)
                    .addContainerGap()))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BotaoSalvar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSalvar2ActionPerformed
        String nome;
        String arquivo[] = new String[25];
        nome = "src/main/java/View/Dados.csv";
        int contador = 0;
        File file = new File(nome);
        try (Scanner scanner = new Scanner(file)) {
            scanner.useDelimiter(",");
            while (scanner.hasNext()) {
                String linha = scanner.nextLine();
                arquivo[contador] = linha;
                contador++;
            }
            if ("Carro".equals(TipoDoVeiculos.getSelectedItem().toString())) {
                String aux = Preco.getText();
                aux += "d";
                double velho = Double.parseDouble(aux);
                arquivo[2] = String.valueOf(velho);
            } else if ("Moto".equals(TipoDoVeiculos.getSelectedItem().toString())) {
                String aux = Preco.getText();
                aux += "d";
                double velho = Double.parseDouble(aux);
                arquivo[5] = String.valueOf(velho);
            } else if ("Caminhão".equals(TipoDoVeiculos.getSelectedItem().toString())) {
                String aux = Preco.getText();
                aux += "d";
                double velho = Double.parseDouble(aux);
                arquivo[8] = String.valueOf(velho);
            } else if ("Van".equals(TipoDoVeiculos.getSelectedItem().toString())) {
                String aux = Preco.getText();
                aux += "d";
                double velho = Double.parseDouble(aux);
                arquivo[11] = String.valueOf(velho);
            }

            try {
                try (FileWriter writer = new FileWriter(nome)) {
                    writer.write(arquivo[0] + "\n" + arquivo[1] + "\n" + arquivo[2] + "\n" + arquivo[3] + "\n" + arquivo[4] + "\n" + arquivo[5] + "\n" + arquivo[6] + "\n" + arquivo[7] + "\n" + arquivo[8] + "\n" + arquivo[9] + "\n" + arquivo[10] + "\n" + arquivo[11] + "\n" + arquivo[12] + "\n" + arquivo[13] + "\n" + arquivo[14] + "\n" + arquivo[15] + "\n" + arquivo[16] + "\n" + arquivo[17] + "\n" + arquivo[18] + "\n" + arquivo[19] + "\n" + arquivo[20] + "\n" + arquivo[21] + "\n"  + arquivo[22] + "\n"  + arquivo[23] + "\n"  + arquivo[24] + "\n");
                }
            } catch (IOException ex) {
                Logger.getLogger(CadastroVeiculo.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CadastroVeiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JOptionPane.showMessageDialog(null, "Novo preço\nVeiculo: "+ TipoDoVeiculos.getSelectedItem().toString() + "\nPreço: "+ Preco.getText());
    }//GEN-LAST:event_BotaoSalvar2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Preco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Preco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Preco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Preco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Preco().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotaoSalvar2;
    private javax.swing.JFormattedTextField Preco;
    private javax.swing.JLabel TextoPainelPreco;
    private javax.swing.JLabel TextoPreco;
    private javax.swing.JLabel TextoTipoDoVeiculo;
    private javax.swing.JComboBox<String> TipoDoVeiculos;
    // End of variables declaration//GEN-END:variables
}
